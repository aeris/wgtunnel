package main

import (
	"context"
	"gopkg.in/yaml.v3"
	"io"
	"log"
	"net"
	"net/http"
	"net/netip"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/urfave/cli/v2"
	"golang.org/x/sync/errgroup"
	"golang.org/x/xerrors"

	"cdr.dev/slog"
	"cdr.dev/slog/sloggers/sloghuman"
	"github.com/coder/wgtunnel/buildinfo"
	"github.com/coder/wgtunnel/tunneld"
	"github.com/coder/wgtunnel/tunnelsdk"
)

func main() {
	cli.VersionFlag = &cli.BoolFlag{
		Name:    "version",
		Aliases: []string{"V"},
		Usage:   "Print the version.",
	}

	app := &cli.App{
		Name:    "tunneld",
		Usage:   "run a wgtunnel server",
		Version: buildinfo.Version(),
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "verbose",
				Aliases: []string{"v"},
				Usage:   "Enable verbose logging.",
				EnvVars: []string{"TUNNELD_VERBOSE"},
			},
			&cli.StringFlag{
				Name:    "config",
				Aliases: []string{"c"},
				Usage:   "ConfigType file to use",
				EnvVars: []string{"TUNNELD_CONFIG_FILE"},
				Value:   "server.yaml",
			},
		},
		Action: runApp,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

type HttpType struct {
	Listen       string `yaml:"listen"`
	BaseUrl      string `yaml:"base-url"`
	RealIpHeader string `yaml:"real-ip-header"`
}

type WireguardType struct {
	Key         string   `yaml:"key"`
	Endpoint    string   `yaml:"endpoint"`
	Mtu         int      `yaml:"mtu"`
	Ip          string   `yaml:"ip"`
	AllowedKeys []string `yaml:"allowed-keys"`
}

type ConfigType struct {
	Http      HttpType      `yaml:"http"`
	Wireguard WireguardType `yaml:"wireguard"`
}

func runApp(ctx *cli.Context) error {

	var (
		verbose    = ctx.Bool("verbose")
		configFile = ctx.String("config")
	)

	logger := slog.Make(sloghuman.Sink(os.Stderr)).Leveled(slog.LevelInfo)
	if verbose {
		logger = logger.Leveled(slog.LevelDebug)
	}

	buff, err := os.ReadFile(configFile)
	if err != nil {
		return xerrors.Errorf("Unable to open config file %q: %w", configFile, err)
	}
	config := ConfigType{
		Wireguard: WireguardType{
			Ip:  tunneld.DefaultWireguardServerIP.String(),
			Mtu: tunneld.DefaultWireguardMTU,
		},
	}
	if err = yaml.Unmarshal(buff, &config); err != nil {
		return xerrors.Errorf("Unable to parse config file %q: %w", configFile, err)
	}

	baseURL := config.Http.BaseUrl
	if baseURL == "" {
		return xerrors.New("http.base-url is required.")
	}
	baseURLParsed, err := url.Parse(baseURL)
	if err != nil {
		return xerrors.Errorf("could not parse http.base-url %q: %w", baseURL, err)
	}

	wireguardEndpoint := config.Wireguard.Endpoint
	if wireguardEndpoint == "" {
		return xerrors.New("wireguard.endpoint is required.")
	}
	wireguardServerIP := config.Wireguard.Ip
	wireguardServerIPParsed, err := netip.ParsePrefix(wireguardServerIP)
	if err != nil {
		return xerrors.Errorf("could not parse wireguard.ip %q: %w", wireguardServerIP, err)
	}

	wireguardKey := config.Wireguard.Key
	if wireguardKey == "" {
		return xerrors.New("wireguard.key is required.")
	}
	wireguardKeyParsed, err := tunnelsdk.ParsePrivateKey(wireguardKey)
	if err != nil {
		return xerrors.Errorf("could not parse wireguard-key %q: %w", wireguardKey, err)
	}
	wireguardPublicKey, _ := wireguardKeyParsed.PublicKey()
	logger.Info(ctx.Context, "parsed private key", slog.F("public-key", wireguardPublicKey))

	wireguardAllowedKeys := config.Wireguard.AllowedKeys
	wireguardAllowedKeysParsed := make([]tunnelsdk.Key, len(wireguardAllowedKeys))
	for n, wireguardAllowedKey := range wireguardAllowedKeys {
		wireguardAllowedKeyParsed, err := tunnelsdk.ParsePublicKey(wireguardAllowedKey)
		if err != nil {
			return xerrors.Errorf("could not parse wireguard.allowed-key %q: %w", wireguardAllowedKey, err)
		}
		wireguardAllowedKeysParsed[n] = wireguardAllowedKeyParsed
	}

	wireguardMTU := config.Wireguard.Mtu
	realIPHeader := config.Http.RealIpHeader

	options := &tunneld.Options{
		BaseURL:              baseURLParsed,
		WireguardEndpoint:    wireguardEndpoint,
		WireguardKey:         wireguardKeyParsed,
		WireguardMTU:         wireguardMTU,
		WireguardServer:      wireguardServerIPParsed,
		WireguardAllowedKeys: wireguardAllowedKeysParsed,
		RealIPHeader:         realIPHeader,
	}
	td, err := tunneld.New(options)
	if err != nil {
		return xerrors.Errorf("create tunneld.API instance: %w", err)
	}

	// ReadHeaderTimeout is purposefully not enabled. It caused some issues with
	// websockets over the dev tunnel.
	// See: https://github.com/coder/coder/pull/3730
	//nolint:gosec
	listenAddress := config.Http.Listen
	parsedUrl, err := url.Parse(listenAddress)
	if err != nil {
		return xerrors.Errorf("couldn't parse listen address %w: %w", listenAddress, err)
	}
	scheme := parsedUrl.Scheme
	var listenArg string
	switch scheme {
	case "unix":
		listenArg = parsedUrl.Path
	case "tcp":
		listenArg = parsedUrl.Host
	}

	server := &http.Server{
		// These errors are typically noise like "TLS: EOF". Vault does similar:
		// https://github.com/hashicorp/vault/blob/e2490059d0711635e529a4efcbaa1b26998d6e1c/command/server.go#L2714
		ErrorLog: log.New(io.Discard, "", 0),
		Handler:  td.Router(),
	}

	eg, egCtx := errgroup.WithContext(ctx.Context)
	eg.Go(func() error {
		logger.Info(egCtx, "listening for requests", slog.F("listen_address", listenAddress))
		listen, err := net.Listen(scheme, listenArg)
		if err != nil {
			return xerrors.Errorf("couldn't listen on %w: %w", listenAddress, err)
		}
		err = server.Serve(listen)
		if err != nil {
			return xerrors.Errorf("error in ListenAndServe: %w", err)
		}
		return nil
	})

	notifyCtx, notifyStop := signal.NotifyContext(ctx.Context, InterruptSignals...)
	defer notifyStop()

	eg.Go(func() error {
		<-notifyCtx.Done()
		logger.Info(egCtx, "shutting down server due to signal")

		shutdownCtx, shutdownCancel := context.WithTimeout(egCtx, 5*time.Second)
		defer shutdownCancel()
		return server.Shutdown(shutdownCtx)
	})

	return eg.Wait()
}
