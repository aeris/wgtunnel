package main

import (
	"context"
	"errors"
	"fmt"
	"gopkg.in/yaml.v3"
	"io"
	"log"
	"net"
	"net/url"
	"os"
	"os/signal"
	"time"

	"github.com/urfave/cli/v2"
	"golang.org/x/xerrors"

	"cdr.dev/slog"
	"cdr.dev/slog/sloggers/sloghuman"
	"github.com/coder/wgtunnel/buildinfo"
	"github.com/coder/wgtunnel/tunnelsdk"
)

func main() {
	cli.VersionFlag = &cli.BoolFlag{
		Name:    "version",
		Aliases: []string{"V"},
		Usage:   "Print the version.",
	}

	app := &cli.App{
		Name:      "tunnel",
		Usage:     "run a wgtunnel client",
		ArgsUsage: "<target-address (e.g. 127.0.0.1:8080)>",
		Version:   buildinfo.Version(),
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "verbose",
				Aliases: []string{"v"},
				Usage:   "Enable verbose logging.",
				EnvVars: []string{"TUNNEL_VERBOSE"},
			},
			&cli.StringFlag{
				Name:    "config",
				Aliases: []string{"c"},
				Usage:   "ConfigType file to use",
				EnvVars: []string{"TUNNELD_CONFIG_FILE"},
				Value:   "client.yaml",
			},
			&cli.StringFlag{
				Name:    "name",
				Aliases: []string{"n"},
				Usage:   "Tunnel name to use",
				EnvVars: []string{"TUNNELD_NAME"},
			},
		},
		Action: runApp,
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

type HttpType struct {
	Endpoint string `yaml:"endpoint"`
}

type WireguardType struct {
	Key string `yaml:"key"`
}

type ConfigType struct {
	Http      HttpType      `yaml:"http"`
	Wireguard WireguardType `yaml:"wireguard"`
}

func runApp(ctx *cli.Context) error {
	var (
		verbose    = ctx.Bool("verbose")
		configFile = ctx.String("config")
		name       = ctx.String("name")
	)

	logger := slog.Make(sloghuman.Sink(os.Stderr)).Leveled(slog.LevelInfo)
	if verbose {
		logger = logger.Leveled(slog.LevelDebug)
	}

	buff, err := os.ReadFile(configFile)
	if err != nil {
		return xerrors.Errorf("Unable to open config file %q: %w", configFile, err)
	}
	config := ConfigType{}
	if err = yaml.Unmarshal(buff, &config); err != nil {
		return xerrors.Errorf("Unable to parse config file %q: %w", configFile, err)
	}

	apiURL := config.Http.Endpoint
	if apiURL == "" {
		return xerrors.New("http.endpoint is required.")
	}
	apiURLParsed, err := url.Parse(apiURL)
	if err != nil {
		return xerrors.Errorf("failed to parse http.endpoint %q: %w", apiURL, err)
	}
	wireguardKey := config.Wireguard.Key
	if wireguardKey == "" {
		return xerrors.New("wireguard.key is required.")
	}
	wireguardKeyParsed, err := tunnelsdk.ParsePrivateKey(wireguardKey)
	if err != nil {
		return xerrors.Errorf("could not parse wireguard.key: %w", err)
	}

	if ctx.Args().Len() != 1 {
		return xerrors.New("exactly one argument (target-address) is required. See --help for more information.")
	}
	targetAddress := ctx.Args().Get(0)
	if targetAddress == "" {
		return xerrors.New("target-address is empty")
	}
	_, _, err = net.SplitHostPort(targetAddress)
	if err != nil {
		return xerrors.Errorf("target-address %q is not a valid host:port: %w", targetAddress, err)
	}

	client := tunnelsdk.New(apiURLParsed)
	tunnel, err := client.LaunchTunnel(ctx.Context, tunnelsdk.TunnelConfig{
		Log:        logger,
		PrivateKey: wireguardKeyParsed,
		Name:       name,
	})
	if err != nil {
		return xerrors.Errorf("launch tunnel: %w", err)
	}
	defer func() {
		err := tunnel.Close()
		if err != nil {
			logger.Error(ctx.Context, "close tunnel", slog.Error(err))
		}
	}()

	// Start forwarding traffic to/from the tunnel.
	go func() {
		for {
			conn, err := tunnel.Listener.Accept()
			if err != nil {
				logger.Error(ctx.Context, "close tunnel", slog.Error(err))
				tunnel.Close()
				return
			}

			go func() {
				defer conn.Close()

				dialCtx, dialCancel := context.WithTimeout(ctx.Context, 10*time.Second)
				defer dialCancel()

				targetConn, err := (&net.Dialer{}).DialContext(dialCtx, "tcp", targetAddress)
				if err != nil {
					logger.Warn(ctx.Context, "could not dial target", slog.F("target_address", targetAddress), slog.Error(err))
					return
				}
				defer targetConn.Close()

				go func() {
					_, err := io.Copy(targetConn, conn)
					if err != nil && !errors.Is(err, io.EOF) {
						logger.Warn(ctx.Context, "could not copy from tunnel to target", slog.Error(err))
					}
				}()

				_, err = io.Copy(conn, targetConn)
				if err != nil && !xerrors.Is(err, io.EOF) {
					logger.Warn(ctx.Context, "could not copy from target to tunnel", slog.Error(err))
				}
			}()
		}
	}()

	_, _ = fmt.Printf("\nTunnel is ready! You can now connect to %s\n", tunnel.URL.String())

	notifyCtx, notifyStop := signal.NotifyContext(ctx.Context, InterruptSignals...)
	defer notifyStop()

	select {
	case <-notifyCtx.Done():
		_, _ = fmt.Printf("\nClosing tunnel due to signal...\n")
		return tunnel.Close()
	case <-tunnel.Wait():
	}

	return nil
}
