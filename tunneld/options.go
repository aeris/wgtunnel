package tunneld

import (
	"encoding/base32"
	"net"
	"net/http"
	"net/netip"
	"net/url"
	"strings"
	"time"

	"golang.org/x/xerrors"

	"cdr.dev/slog"
	"github.com/coder/wgtunnel/tunnelsdk"
)

const (
	DefaultWireguardMTU     = 1280
	DefaultPeerDialTimeout  = 10 * time.Second
	DefaultPeerPollDuration = 30 * time.Second
	DefaultPeerTimeout      = 2 * time.Minute
)

var (
	DefaultWireguardServerIP = netip.MustParsePrefix("fcca::1/16")
)

var newHostnameEncoder = base32.HexEncoding.WithPadding(base32.NoPadding)

type Options struct {
	Log slog.Logger

	// BaseURL is the base URL to use for the tunnel, including scheme. All
	// tunnels will be subdomains of this hostname.
	// e.g. "https://tunnel.example.com" will place tunnels at
	//      "https://xyz.tunnel.example.com"
	BaseURL *url.URL

	// WireguardEndpoint is the UDP address advertised to clients that they will
	// connect to for wireguard connections. It should be in the form
	// "$ip:$port" or "$hostname:$port".
	WireguardEndpoint string
	// WireguardKey is the private key for the wireguard server.
	WireguardKey tunnelsdk.Key
	// WireguardAllowedKeys is the private keys allowed to use the wireguard server.
	WireguardAllowedKeys []tunnelsdk.Key

	// WireguardMTU is the MTU to use for the wireguard interface. Defaults to
	// 1280.
	WireguardMTU int
	// WireguardServer is the virtual IP address of this server in the
	// wireguard network. Must be an IPv6 address with a CIDR and have at least
	// 64 bits of space available. Defaults to fcca::1/16.
	WireguardServer netip.Prefix

	// RealIPHeader is the header to use for getting a request's IP address. If
	// not set, the request's RemoteAddr will be used.
	//
	// Used for rate limiting.
	RealIPHeader string

	// PeerDialTimeout is the timeout for dialing a peer on a request. Defaults
	// to 10 seconds.
	PeerDialTimeout time.Duration

	// PeerRegisterInterval is how often the clients should re-register.
	PeerRegisterInterval time.Duration

	// PeerTimeout is how long the server will wait before removing the peer.
	PeerTimeout time.Duration
}

// Validate checks that the options are valid and populates default values for
// missing fields.
func (options *Options) Validate() error {
	if options == nil {
		return xerrors.New("options is nil")
	}
	if options.BaseURL == nil {
		return xerrors.New("BaseURL is required")
	}
	if options.WireguardEndpoint == "" {
		return xerrors.New("WireguardEndpoint is required")
	}
	_, _, err := net.SplitHostPort(options.WireguardEndpoint)
	if err != nil {
		return xerrors.Errorf("WireguardEndpoint %q is not a valid host:port combination: %w", options.WireguardEndpoint, err)
	}
	if options.WireguardKey.IsZero() {
		return xerrors.New("WireguardKey is required")
	}
	if !options.WireguardKey.IsPrivate() {
		return xerrors.New("WireguardKey must be a private key")
	}
	// Key is parsed and validated when the server is started.
	if options.WireguardMTU <= 0 {
		options.WireguardMTU = DefaultWireguardMTU
	}
	if (options.WireguardServer == netip.Prefix{}) {
		options.WireguardServer = DefaultWireguardServerIP
	}
	if !options.WireguardServer.Addr().Is6() {
		return xerrors.New("WireguardServer must be an IPv6 address")
	}
	if options.WireguardServer.Bits() > 64 {
		return xerrors.New("WireguardServer must have at least 64 bits available")
	}
	if options.WireguardServer.Bits()%8 != 0 {
		return xerrors.New("WireguardServer must be a multiple of 8 bits")
	}

	if options.RealIPHeader != "" {
		options.RealIPHeader = http.CanonicalHeaderKey(options.RealIPHeader)
	}

	if options.PeerDialTimeout <= 0 {
		options.PeerDialTimeout = DefaultPeerDialTimeout
	}
	if options.PeerRegisterInterval <= 0 {
		options.PeerRegisterInterval = DefaultPeerPollDuration
	}
	if options.PeerTimeout <= 0 {
		options.PeerTimeout = DefaultPeerTimeout
	}
	if options.PeerRegisterInterval >= options.PeerTimeout {
		return xerrors.Errorf("PeerRegisterInterval(%s) must be less than PeerTimeout(%s)",
			options.PeerRegisterInterval.String(),
			options.PeerTimeout.String(),
		)
	}

	return nil
}

func (options *Options) URLFromName(name string) url.URL {
	formatURL := *options.BaseURL
	formatURL.Host = strings.ToLower(name) + "." + formatURL.Host
	return formatURL
}

// WireguardPublicKeyToIPAndURLs returns the IP address that corresponds to the
// given wireguard public key, as well as all accepted tunnel URLs for the key.
//
// We support an older 32 character format ("old format") and a newer 12
// character format ("good format") which is preferred. The first URL returned
// should be considered "preferred", and all other URLs are provided for
// compatibility with older deployments only. The "good format" is preferred as
// it's shorter to avoid issues with hostname length limits when apps prefixes
// are added to the equation.
//
// "good format":
//
//	Take the first 8 bytes of the hash of the public key, and convert to
//	base32.
//
// "old format":
//
//	Take the network prefix, and create a new address filling the last n bytes
//	with the first n bytes of the hash of the public key. Then convert to hex.
func (options *Options) WireguardPublicKeyToIPAndURLs(publicKey tunnelsdk.Key) (netip.Addr, url.URL) {
	var (
		keyHash   = publicKey.Hash()
		addrBytes = options.WireguardServer.Addr().As16()
	)

	// IPv6 address:
	// For the IP address, we take the first 64 bits of the network prefix and
	// the first 64 bits of the hash of the public key.
	copy(addrBytes[8:], keyHash[:8])

	formatBytes := make([]byte, 8)
	copy(formatBytes, keyHash[:8])
	format := newHostnameEncoder.EncodeToString(formatBytes)

	return netip.AddrFrom16(addrBytes), options.URLFromName(format)
}
