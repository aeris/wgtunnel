package tunneld_test

import (
	"fmt"
	"net/netip"
	"net/url"
	"strings"
	"testing"
	"time"

	"github.com/coder/wgtunnel/tunneld"
	"github.com/coder/wgtunnel/tunnelsdk"
	"github.com/stretchr/testify/require"
)

func Test_Option(t *testing.T) {
	t.Parallel()

	key, err := tunnelsdk.GeneratePrivateKey()
	require.NoError(t, err)

	t.Run("Validate", func(t *testing.T) {
		t.Parallel()

		t.Run("FullValid", func(t *testing.T) {
			t.Parallel()

			o := tunneld.Options{
				BaseURL: &url.URL{
					Scheme: "http",
					Host:   "localhost",
				},
				WireguardEndpoint:    "localhost:1234",
				WireguardKey:         key,
				WireguardMTU:         tunneld.DefaultWireguardMTU + 1,
				WireguardServer:      netip.MustParsePrefix("feed::1/64"),
				RealIPHeader:         "X-Real-Ip",
				PeerDialTimeout:      1 * time.Second,
				PeerRegisterInterval: time.Second,
				PeerTimeout:          2 * time.Second,
			}

			clone := o
			clone.BaseURL = &url.URL{
				Scheme: o.BaseURL.Scheme,
				Host:   o.BaseURL.Host,
			}
			clonePtr := &clone
			err := clonePtr.Validate()
			require.NoError(t, err)

			// Should not have updated the struct.
			require.Equal(t, o, clone)
		})

		t.Run("Valid", func(t *testing.T) {
			t.Parallel()

			o := &tunneld.Options{
				BaseURL: &url.URL{
					Scheme: "http",
					Host:   "localhost",
				},
				WireguardEndpoint: "localhost:1234",
				WireguardKey:      key,
				RealIPHeader:      "x-real-ip",
			}

			err := o.Validate()
			require.NoError(t, err)

			require.Equal(t, &url.URL{Scheme: "http", Host: "localhost"}, o.BaseURL)
			require.Equal(t, "localhost:1234", o.WireguardEndpoint)
			require.Equal(t, key, o.WireguardKey)
			require.EqualValues(t, tunneld.DefaultWireguardMTU, o.WireguardMTU)
			require.Equal(t, tunneld.DefaultWireguardServerIP, o.WireguardServer)
			// should be canonicalized.
			require.Equal(t, "X-Real-Ip", o.RealIPHeader)
		})

		t.Run("Invalid", func(t *testing.T) {
			t.Parallel()

			t.Run("Nil", func(t *testing.T) {
				t.Parallel()

				err := (*tunneld.Options)(nil).Validate()
				require.Error(t, err)
				require.ErrorContains(t, err, "options is nil")
			})

			t.Run("BaseURL", func(t *testing.T) {
				t.Parallel()

				o := &tunneld.Options{
					BaseURL:           nil,
					WireguardEndpoint: "localhost:1234",
					WireguardKey:      key,
				}

				err := o.Validate()
				require.Error(t, err)
				require.ErrorContains(t, err, "BaseURL is required")
			})

			t.Run("WireguardEndpoint", func(t *testing.T) {
				t.Parallel()

				o := &tunneld.Options{
					BaseURL: &url.URL{
						Scheme: "http",
						Host:   "localhost",
					},
					WireguardEndpoint: "",
					WireguardKey:      key,
				}

				err := o.Validate()
				require.Error(t, err)
				require.ErrorContains(t, err, "WireguardEndpoint is required")

				o.WireguardEndpoint = "localhost"

				err = o.Validate()
				require.Error(t, err)
				require.ErrorContains(t, err, "not a valid host:port")
			})

			t.Run("WireguardKey", func(t *testing.T) {
				t.Parallel()

				o := &tunneld.Options{
					BaseURL: &url.URL{
						Scheme: "http",
						Host:   "localhost",
					},
					WireguardEndpoint: "localhost:1234",
					WireguardKey:      tunnelsdk.Key{},
				}

				err := o.Validate()
				require.Error(t, err)
				require.ErrorContains(t, err, "WireguardKey is required")

				o.WireguardKey, err = key.PublicKey()
				require.NoError(t, err)

				err = o.Validate()
				require.Error(t, err)
				require.ErrorContains(t, err, "WireguardKey must be a private key")
			})

			t.Run("WireguardServer", func(t *testing.T) {
				t.Parallel()

				o := &tunneld.Options{
					BaseURL: &url.URL{
						Scheme: "http",
						Host:   "localhost",
					},
					WireguardEndpoint: "localhost:1234",
					WireguardKey:      key,
					WireguardServer:   netip.MustParsePrefix("127.0.0.1/16"),
				}

				err := o.Validate()
				require.Error(t, err)
				require.ErrorContains(t, err, "WireguardServer must be an IPv6 address")
			})

			t.Run("WireguardServer", func(t *testing.T) {
				t.Parallel()

				o := &tunneld.Options{
					BaseURL: &url.URL{
						Scheme: "http",
						Host:   "localhost",
					},
					WireguardEndpoint: "localhost:1234",
					WireguardKey:      key,
					WireguardServer:   netip.MustParsePrefix("feed::1/128"),
				}

				err := o.Validate()
				require.Error(t, err)
				require.ErrorContains(t, err, "WireguardServer must have at least 64 bits available")
			})
		})
	})

	t.Run("WireguardPublicKeyToIPAndURLs", func(t *testing.T) {
		t.Parallel()

		cases := []struct {
			// base64 encoded
			key string
			ip  string
			url string
		}{
			{
				key: "8HGwtvNSGqXyO2s7UCW/NtvQM7L5jUL+s76h3qZbeG0=",
				ip:  "cd01:15ef:efd7:b8a9",
				url: "http://pk0hbrvfqusai.localhost.com",
			},
			{
				key: "ikEH8jCTwDMpQb7B1SbLi7itzDHJrlLzZtdNmuiLZHo=",
				ip:  "7bd2:93ac:ae4b:a6b4",
				url: "http://ff997b5e9ejb8.localhost.com",
			},
			{
				key: "8yxYMm//sfv27tkSz9itIa/8Ihql+vFRpsvjTSTaYAg=",
				ip:  "186a:e14b:954f:99fa",
				url: "http://31le2isl9ucvk.localhost.com",
			},
			{
				key: "Gl7xZzfkCyFTbB+Uejc17GmfbjLy6s8NEZBaJKx/swU=",
				ip:  "7eb3:bd05:c798:6293",
				url: "http://fqprq1e7j1h96.localhost.com",
			},
			{
				key: "f8YjkcGgOggYzlIr2KtShY+8ZgR0hIXmJHPjCG8wi2Q=",
				ip:  "edab:e28b:9842:8332",
				url: "http://tmlu52so8a1j4.localhost.com",
			},
			{
				key: "Q3dubFlwwLnCpQTagjCckb1XLGtViZoBX1qHAZWV2gI=",
				ip:  "2a0c:1537:9ff4:5fc5",
				url: "http://5861adsvuhfsa.localhost.com",
			},
		}

		for i, c := range cases {
			i, c := i, c

			pubKey, err := tunnelsdk.ParsePublicKey(c.key)
			require.NoError(t, err)

			t.Run(fmt.Sprintf("Default/%d", i), func(t *testing.T) {
				t.Parallel()

				options := &tunneld.Options{
					BaseURL: &url.URL{
						Scheme: "http",
						Host:   "localhost.com",
					},
					WireguardEndpoint: "localhost:1234",
					WireguardKey:      key,
					WireguardServer:   tunneld.DefaultWireguardServerIP,
				}
				err := options.Validate()
				require.NoError(t, err)

				expectedIP := "fcca::" + c.ip

				ip, url := options.WireguardPublicKeyToIPAndURLs(pubKey)
				require.Equal(t, expectedIP, ip.String())
				require.Equal(t, c.url, url.String())
			})

			t.Run(fmt.Sprintf("LongerPrefix/%d", i), func(t *testing.T) {
				t.Parallel()

				options := &tunneld.Options{
					BaseURL: &url.URL{
						Scheme: "http",
						Host:   "localhost.com",
					},
					WireguardEndpoint: "localhost:1234",
					WireguardKey:      key,
					WireguardServer:   netip.MustParsePrefix("feed:beef:deaf:deed::1/64"),
				}
				err := options.Validate()
				require.NoError(t, err)

				expectedIP := "feed:beef:deaf:deed:" + c.ip

				// The second URL has a different IP prefix length, so adjust
				// accordingly.
				expectedURL2, err := url.Parse(c.url)
				require.NoError(t, err)
				hostRest := strings.SplitN(expectedURL2.Host, ".", 2)[1]
				expectedURL2.Host = "feedbeefdeafdeed" + expectedURL2.Host[4:20] + "." + hostRest
				t.Logf("mutated URL %q to %q", c.url, expectedURL2.String())
				expectedURL := c.url

				ip, url := options.WireguardPublicKeyToIPAndURLs(pubKey)
				require.Equal(t, expectedIP, ip.String())
				require.Equal(t, expectedURL, url.String())
			})
		}
	})

	t.Run("HostnameToWireguardIP", func(t *testing.T) {
		t.Parallel()

		cases := []struct {
			hostname    string
			ip          string
			errContains string
		}{
			// Good format:
			{
				hostname: "v2vphj9slsv64",
				ip:       "f8bf:98cd:3caf:3e62",
			},
			{
				hostname: "458c5qhovo11u",
				ip:       "2150:c2ea:38fe:21f",
			},
			{
				hostname: "o5v75p655qjc8",
				ip:       "c17e:72e4:c52e:a6c4",
			},
			{
				hostname: "utpis23n3lt6u",
				ip:       "f773:2e08:771d:7a6f",
			},
			{
				hostname: "rjokstglnmpce",
				ip:       "dcf1:4e76:15bd:b2c7",
			},
			{
				hostname: "4mh8kgpei4ak6",
				ip:       "25a2:8a43:2e91:1543",
			},

			// Good format errors:
			{
				hostname:    "v2vphj9slsv64.localhost.com",
				errContains: "decode new hostname",
			},
			{
				hostname:    "4mh8kgpei4ak64mh8kgpei4ak6",
				errContains: "invalid new hostname length",
			},

			// Bad format:
			{
				hostname: "fccaf8bf98cd3caf3e6270a5db3140f9",
				ip:       "f8bf:98cd:3caf:3e62",
			},
			{
				hostname: "fcca2150c2ea38fe021f76fac00cd533",
				ip:       "2150:c2ea:38fe:21f",
			},
			{
				hostname: "fccac17e72e4c52ea6c4fbb4ef809339",
				ip:       "c17e:72e4:c52e:a6c4",
			},
			{
				hostname: "fccaf7732e08771d7a6f6fdcb4a1f367",
				ip:       "f773:2e08:771d:7a6f",
			},
			{
				hostname: "fccadcf14e7615bdb2c7638238302374",
				ip:       "dcf1:4e76:15bd:b2c7",
			},
			{
				hostname: "fcca25a28a432e9115439264ae85af84",
				ip:       "25a2:8a43:2e91:1543",
			},

			// Bad format errors:
			{
				hostname:    "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
				errContains: "decode old hostname",
			},
		}

		for i, c := range cases {
			c := c

			t.Run(fmt.Sprint(i), func(t *testing.T) {
				t.Parallel()

				t.Run("Default", func(t *testing.T) {
					t.Parallel()

					options := &tunneld.Options{
						BaseURL: &url.URL{
							Scheme: "http",
							Host:   "localhost.com",
						},
						WireguardEndpoint: "localhost:1234",
						WireguardKey:      key,
						WireguardServer:   tunneld.DefaultWireguardServerIP,
					}
					err := options.Validate()
					require.NoError(t, err)
				})

				t.Run("LongerPrefix", func(t *testing.T) {
					t.Parallel()

					options := &tunneld.Options{
						BaseURL: &url.URL{
							Scheme: "http",
							Host:   "localhost.com",
						},
						WireguardEndpoint: "localhost:1234",
						WireguardKey:      key,
						WireguardServer:   netip.MustParsePrefix("feed:beef:deaf:deed::1/64"),
					}
					err := options.Validate()
					require.NoError(t, err)

					// The second hostname has a different IP prefix length, so
					// adjust accordingly.
					hostname := c.hostname
					if len(hostname) == 32 {
						hostname = "feedbeefdeafdeed" + hostname[4:20]
						t.Logf("mutated hostname %q to %q", c.hostname, hostname)
					}
				})
			})
		}
	})
}
