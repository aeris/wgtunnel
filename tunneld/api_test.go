package tunneld_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"

	"github.com/coder/wgtunnel/tunnelsdk"
)

func Test_postClients(t *testing.T) {
	t.Parallel()

	td, client := createTestTunneld(t, nil)

	key, err := tunnelsdk.GeneratePrivateKey()
	pubKey, _ := key.PublicKey()
	require.NoError(t, err)

	expectedIP, expectedURL := td.WireguardPublicKeyToIPAndURLs(pubKey)

	// Register a client.
	req := tunnelsdk.ClientRegisterRequest{
		PublicKey: pubKey,
	}
	res, err := client.ClientRegister(context.Background(), req)
	require.NoError(t, err)

	pubKey, _ = td.WireguardKey.PublicKey()
	require.Equal(t, expectedURL.String(), res.TunnelURL)
	require.Equal(t, expectedIP, res.ClientIP)
	require.Equal(t, td.WireguardEndpoint, res.ServerEndpoint)
	require.Equal(t, td.WireguardServer.Addr(), res.ServerIP)
	require.Equal(t, pubKey, res.ServerPublicKey)
	require.Equal(t, td.WireguardMTU, res.WireguardMTU)
	require.Equal(t, td.PeerRegisterInterval, res.ReregisterWait)

	// Register the same client again.
	res2, err := client.ClientRegister(context.Background(), req)
	require.NoError(t, err)
	require.Equal(t, res, res2)
}
