package tunneld

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"net/http/httputil"
	"net/netip"
	"slices"
	"strings"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/hostrouter"
	"golang.org/x/xerrors"

	"github.com/coder/wgtunnel/tunneld/httpapi"
	"github.com/coder/wgtunnel/tunneld/httpmw"
	"github.com/coder/wgtunnel/tunnelsdk"
)

func (api *API) Router() http.Handler {
	var (
		hr            = hostrouter.New()
		apiRouter     = chi.NewRouter()
		proxyRouter   = chi.NewRouter()
		unknownRouter = chi.NewRouter()
	)

	hr.Map(api.BaseURL.Host, apiRouter)
	hr.Map("*."+api.BaseURL.Host, proxyRouter)
	hr.Map("*", unknownRouter)

	proxyRouter.Use(
		httpmw.LimitBody(50 * 1 << 20), // 50MB
	)
	proxyRouter.Mount("/", http.HandlerFunc(api.handleTunnel))

	apiRouter.Use(
		httpmw.LimitBody(1<<20), // 1MB
		httpmw.RateLimit(httpmw.RateLimitConfig{
			Log:          api.Log.Named("ratelimier"),
			Count:        10,
			Window:       10 * time.Second,
			RealIPHeader: api.Options.RealIPHeader,
		}),
	)

	apiRouter.Post("/api/v2/clients", api.postClients)

	notFound := func(rw http.ResponseWriter, r *http.Request) {
		httpapi.Write(r.Context(), rw, http.StatusNotFound, tunnelsdk.Response{
			Message: "Not found.",
		})
	}
	apiRouter.NotFound(notFound)
	unknownRouter.NotFound(notFound)

	return hr
}

func (api *API) postClients(rw http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	var req tunnelsdk.ClientRegisterRequest
	if !httpapi.Read(r.Context(), rw, r, &req) {
		return
	}

	if len(api.Options.WireguardAllowedKeys) > 0 && !slices.Contains(api.Options.WireguardAllowedKeys, req.PublicKey) {
		httpapi.Write(ctx, rw, http.StatusUnauthorized, tunnelsdk.Response{
			Message: fmt.Sprintf("public key %s not allowed", req.PublicKey),
		})
		return
	}

	resp, _, err := api.registerClient(req)
	if err != nil {
		httpapi.Write(ctx, rw, http.StatusInternalServerError, tunnelsdk.Response{
			Message: "Failed to register client.",
			Detail:  err.Error(),
		})
		return
	}

	httpapi.Write(ctx, rw, http.StatusOK, resp)
}

func (api *API) registerClient(req tunnelsdk.ClientRegisterRequest) (tunnelsdk.ClientRegisterResponse, bool, error) {
	ip, url := api.WireguardPublicKeyToIPAndURLs(req.PublicKey)
	noisePubKey := req.PublicKey.NoisePublicKey()

	if req.Name != "" {
		url = api.Options.URLFromName(req.Name)
	}

	api.pkeyCacheMu.Lock()
	api.pkeyCache[ip] = cachedPeer{
		key:           noisePubKey,
		lastHandshake: time.Now(),
		url:           url,
	}
	api.pkeyCacheMu.Unlock()

	exists := true
	if api.wgDevice.LookupPeer(noisePubKey) == nil {
		exists = false

		api.pkeyCacheMu.Lock()
		api.pkeyCache[ip] = cachedPeer{
			key:           noisePubKey,
			lastHandshake: time.Now(),
			url:           url,
		}
		api.pkeyCacheMu.Unlock()

		err := api.wgDevice.IpcSet(fmt.Sprintf("public_key=%x\nallowed_ip=%s/128",
			noisePubKey, ip.String(),
		))
		if err != nil {
			return tunnelsdk.ClientRegisterResponse{}, false, xerrors.Errorf("register client with wireguard: %w", err)
		}
	}

	pubKey, _ := api.WireguardKey.PublicKey()
	return tunnelsdk.ClientRegisterResponse{
		ReregisterWait:  api.PeerRegisterInterval,
		TunnelURL:       url.String(),
		ClientIP:        ip,
		ServerEndpoint:  api.WireguardEndpoint,
		ServerIP:        api.WireguardServer.Addr(),
		ServerPublicKey: pubKey,
		WireguardMTU:    api.WireguardMTU,
	}, exists, nil
}

type ipPortKey struct{}

func normalizeHost(host string) string {
	parts := strings.SplitN(host, ".", 2)
	subdomain, domain := parts[0], parts[1]
	subdomainParts := strings.Split(subdomain, "-")
	subdomain = subdomainParts[len(subdomainParts)-1]
	parts = []string{subdomain, domain}
	return strings.Join(parts, ".")
}

func (api *API) hostnameToWireguardIP(r *http.Request) (netip.Addr, cachedPeer, bool) {
	host := normalizeHost(r.Host)
	for ip, peer := range api.pkeyCache {
		if peer.url.Host == host {
			return ip, peer, true
		}
	}
	return netip.Addr{}, cachedPeer{}, false
}

func (api *API) handleTunnel(rw http.ResponseWriter, r *http.Request) {
	ctx := r.Context()

	ip, pkey, ok := api.hostnameToWireguardIP(r)
	if !ok {
		httpapi.Write(ctx, rw, http.StatusBadRequest, tunnelsdk.Response{
			Message: "Invalid tunnel URL.",
		})
		return
	}

	if time.Since(pkey.lastHandshake) > api.PeerTimeout {
		httpapi.Write(ctx, rw, http.StatusBadGateway, tunnelsdk.Response{
			Message: "Peer is not connected.",
			Detail:  "",
		})
		return
	}

	// The transport on the reverse proxy uses this ctx value to know which
	// IP to dial. See tunneld.go.
	ctx = context.WithValue(ctx, ipPortKey{}, netip.AddrPortFrom(ip, tunnelsdk.TunnelPort))
	r = r.WithContext(ctx)

	rp := httputil.ReverseProxy{
		// This can only happen when it fails to dial.
		ErrorHandler: func(w http.ResponseWriter, r *http.Request, err error) {
			httpapi.Write(ctx, rw, http.StatusBadGateway, tunnelsdk.Response{
				Message: "Failed to dial peer.",
				Detail:  err.Error(),
			})
		},
		Director: func(rp *http.Request) {
			rp.URL.Scheme = "http"
			rp.URL.Host = r.Host
			rp.Host = r.Host
		},
		Transport: api.transport,
	}

	rp.ServeHTTP(rw, r)
}

// splitHostname splits a hostname into the subdomain and the rest of the
// string, stripping any port data and leading/trailing periods.
func splitHostname(hostname string) (subdomain string, rest string) {
	hostname = strings.Trim(hostname, ".")
	hostnameHost, _, err := net.SplitHostPort(hostname)
	if err == nil {
		hostname = hostnameHost
	}

	parts := strings.SplitN(hostname, ".", 2)
	if len(parts) != 2 {
		return hostname, ""
	}

	return parts[0], parts[1]
}
